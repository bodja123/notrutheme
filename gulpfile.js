// ## Globals
var argv         = require('minimist')(process.argv.slice(2));
var autoprefixer = require('gulp-autoprefixer');
var browserSync  = require('browser-sync').create();
var changed      = require('gulp-changed');
var concat       = require('gulp-concat');
var flatten      = require('gulp-flatten');
var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var imagemin     = require('gulp-imagemin');
var jshint       = require('gulp-jshint');
var lazypipe     = require('lazypipe');
var less         = require('gulp-less');
var merge        = require('merge-stream');
var minifyCss    = require('gulp-minify-css');
var plumber      = require('gulp-plumber');
var rev          = require('gulp-rev');
var runSequence  = require('run-sequence');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var uglify       = require('gulp-uglify');
var uncss        = require('gulp-uncss');


// See https://github.com/austinpray/asset-builder
var manifest = require('asset-builder')('./assets/manifest.json');

// `path` - Paths to base asset directories. With trailing slashes.
// - `path.source` - Path to the source files. Default: `assets/`
// - `path.dist` - Path to the build directory. Default: `dist/`
var path = manifest.paths;

// `config` - Store arbitrary configuration values here.
var config = manifest.config || {};

// `globs` - These ultimately end up in their respective `gulp.src`.
// - `globs.js` - Array of asset-builder JS dependency objects. Example:
//   ```
//   {type: 'js', name: 'main.js', globs: []}
//   ```
// - `globs.css` - Array of asset-builder CSS dependency objects. Example:
//   ```
//   {type: 'css', name: 'main.css', globs: []}
//   ```
// - `globs.fonts` - Array of font path globs.
// - `globs.images` - Array of image path globs.
// - `globs.bower` - Array of all the main Bower files.
var globs = manifest.globs;

// `project` - paths to first-party assets.
// - `project.js` - Array of first-party JS assets.
// - `project.css` - Array of first-party CSS assets.
var project = manifest.getProjectGlobs();

// CLI options
var enabled = {
  // Enable static asset revisioning when `--production`
  rev: argv.production,
  // Disable source maps when `--production`
  maps: !argv.production,
  // Fail styles task on error when `--production`
  failStyleTask: argv.production,
  // Fail due to JSHint warnings only when `--production`
  failJSHint: argv.production,
  // Strip debug statments from javascript when `--production`
  stripJSDebug: argv.production
};

// Path to the compiled assets manifest in the dist directory
var revManifest = path.dist + 'assets.json';

// ## Reusable Pipelines
// See https://github.com/OverZealous/lazypipe

// ### CSS processing pipeline
// Example
// ```
// gulp.src(cssFiles)
//   .pipe(cssTasks('main.css')
//   .pipe(gulp.dest(path.dist + 'styles'))
// ```
var cssTasks = function(filename) {
  return lazypipe()
    .pipe(function() {
      return gulpif(!enabled.failStyleTask, plumber());
    })
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.init());
    })
    .pipe(function() {
      return gulpif('*.less', less());
    })
    .pipe(function() {
      return gulpif('*.scss', sass({
        outputStyle: 'nested', // libsass doesn't support expanded yet
        precision: 10,
        includePaths: ['.'],
        errLogToConsole: !enabled.failStyleTask
      }));
    })
    .pipe(concat, filename)

    .pipe(autoprefixer, {
      browsers: [
        'last 2 versions',
        'ie 8',
        'ie 9',
        'android 2.3',
        'android 4',
        'opera 12'
      ]
    })
/*
.pipe(uncss, { html: ["http:\/\/localhost\/notruck\/app","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/ctek-56-308-mxs-5-0-test-und-ladegeraet\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/test-omu\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/tetra-test-6in1-wassertest-fuer-das-aquarium-schnelle-und-einfache-ueberpruefung-der-wasserqualitaet-1-dose-25-teststreifen\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/walther-p99-schwarz-mit-2-magazinen\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/survival-guide-dieses-buch-koennte-ihr-leben-retten\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/intipal-kurbeldynamo-solar-radio-solarradio-mit-kurbel-led-taschenlampe-notfall-usb-handy-ladegeraet-camping-outdoor\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/aerocase-pro1r-bl1-notfalltasche-polyester-gr-l-rettungsdienst-notfall-rucksack-notfalnotfalltasche\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/liebeskummerpillen-pocket-chocolate-notfallschokolade_blechdose-1er-pack-1-x-30-g\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/jarbo-multifunctional-emergency-solar-wind-up-self-powered-and-rechargeable-amfmnoaa-weather-radio-use-as-led-flashlight-and-smart-phone-charger-power-bank-red\/","http:\/\/localhost\/notruck\/app\/index.php\/datenschutzerklaerung\/","http:\/\/localhost\/notruck\/app\/index.php\/impressum\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/hawk-outdoor-ultra-light-reise-haengematte-aus-fallschirm-seide-200kg-traglast-set-mit-befestigung-2-karabiner-und-2-seile-reise-camping-garten-trekking-strand-travel-hammock\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/the-friendly-swede-survival-pod-survival-kit-inklusive-drahtsaege-rettungsdecke-und-paracord\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/semptec-urban-survival-technology-notfall-schlaf-und-rettungssack\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/survival-ueberall-ueberleben-als-normaler-stadtbewohner\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/pflanzliche-notnahrung-survivalwissen-fuer-extremsituationen\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/set-mit-zwei-survival-armbaendern-paracord-mit-pfeife-feuerstein-schaber-zum-feuer-machen-fuer-aktivitaeten-im-freien-schwarz-armee-gruen\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/das-grosse-buch-der-ueberlebenstechniken\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/10-in-1-selbsthilfe-aussen-survival-notfall-self-help-sport-camping-wandern-werkzeuge-box-kit-set\/","http:\/\/localhost\/notruck\/app\/index.php\/produkt\/red-cherry-wimpern-2x-nr-43-nr-2x-747s-und-eyelash-adhesive-set\/","http:\/\/localhost\/notruck\/app\/index.php\/2015\/11\/19\/wie-lange-dauert-die-lieferzeit\/","http:\/\/localhost\/notruck\/app\/index.php\/faq-haeufig-gestellte-fragen\/","http:\/\/localhost\/notruck\/app\/index.php\/2015\/10\/03\/recon-88\/","http:\/\/localhost\/notruck\/app\/index.php\/checkout\/","http:\/\/localhost\/notruck\/app\/index.php\/my-account\/","http:\/\/localhost\/notruck\/app\/index.php\/shop\/","http:\/\/localhost\/notruck\/app\/index.php\/cart\/","http:\/\/localhost\/notruck\/app\/","http:\/\/localhost\/notruck\/app\/index.php\/2015\/07\/23\/hello-world\/","http:\/\/localhost\/notruck\/app\/index.php\/ratgeber\/","http:\/\/localhost\/notruck\/app\/index.php\/author\/bodja\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/0\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/0\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/0603983997284\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/0603983997284\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/0609788926535\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/0609788926535\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/0706238604309\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/0706238604309\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/0712038029112\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/0712038029112\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_numberofitems\/1\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagequantity\/1\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_edition\/1\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/102-hundredths-inches-453-hundredths-inches-1-hundredths-pounds-276-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/1035-hundredths-inches-776-hundredths-inches-122-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/110-hundredths-inches-1016-hundredths-inches-265-hundredths-pounds-772-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/1142-hundredths-inches-197-hundredths-inches-132-hundredths-pounds-591-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/115-hundredths-pounds\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/118-hundredths-inches-654-hundredths-inches-9-hundredths-pounds-638-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_isbn\/1523964774\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/165-hundredths-inches-472-hundredths-inches-13-hundredths-pounds-472-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/165-hundredths-inches-780-hundredths-inches-66-hundredths-pounds-575-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/197-hundredths-inches-441-hundredths-inches-22-hundredths-pounds-291-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_edition\/2\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publicationdate\/2007-01-10\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publicationdate\/2010-02-01\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publicationdate\/2014-01-29\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publicationdate\/2014-05-30\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publicationdate\/2016-02-09\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_numberofpages\/212\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/217-hundredths-inches-630-hundredths-inches-71-hundredths-pounds-394-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/220-hundredths-inches-1079-hundredths-inches-185-hundredths-pounds-575-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/220-hundredths-inches-906-hundredths-inches-212-hundredths-pounds-630-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/236-hundredths-inches-512-hundredths-inches-157-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_numberofpages\/240\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_size\/25-stueck\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_size\/260x140cm\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagequantity\/3\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_catalognumberlist\/307334\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_numberofpages\/320\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_isbn\/3613507633\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/374-hundredths-inches-945-hundredths-inches-119-hundredths-pounds-575-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_isbn\/3831016267\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_isbn\/3937872485\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/394-hundredths-inches-630-hundredths-inches-71-hundredths-pounds-217-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/4022107087131\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/4022107087131\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/4031846011130\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/4031846011130\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/4260042691116\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/4260042691116\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/454\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/454\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/472-hundredths-inches-150-hundredths-inches-8-hundredths-pounds-472-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/48-hundredths-inches-900-hundredths-inches-84-hundredths-pounds-600-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_numberofpages\/506\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/5060385171677\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/5060385171677-5057065042013-4004218175488-5057065162773-5054480186216\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/58-hundredths-inches-710-hundredths-inches-18-hundredths-pounds-542-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upc\/603983997284\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upclist\/603983997284\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upc\/609788926535\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upclist\/609788926535\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/63-hundredths-inches-1110-hundredths-inches-2-hundredths-pounds-252-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/63-hundredths-inches-811-hundredths-inches-106-hundredths-pounds-551-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upc\/706238604309\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upclist\/706238604309\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upc\/712038029112\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_upclist\/712038029112\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/7350009563086\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/7350009563086\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/760-hundredths-inches-539-hundredths-inches-67-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_model\/8157\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/8157\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/8157\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/819-hundredths-inches-846-hundredths-inches-106-hundredths-pounds-67-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_edition\/9\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/900-hundredths-inches-600-hundredths-inches-48-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_itemdimensions\/909-hundredths-inches-626-hundredths-inches-176-hundredths-pounds-197-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_packagedimensions\/94-hundredths-inches-449-hundredths-inches-18-hundredths-pounds-268-hundredths-inches\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/978-3-613-50763-0\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/978-3-613-50763-0\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/9781523964772\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/9781523964772\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/9783613507630\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/9783613507630\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/9783831016266\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/9783831016266\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/9783831016266\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/9783831016266\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/9783937872483\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/9783937872483\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_ean\/9823219020505\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_eanlist\/9823219020505\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/abis_book\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/abis_dvd\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/ausruestung\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/auto_accessory\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/automotive\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/automotive-parts-and-accessories\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/badartikel\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/beauty\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/beauty\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/book\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/ce\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_author\/colin-towell\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/createspace-independent-publishing-platform\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/createspace-independent-publishing-platform\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/createspace-independent-publishing-platform\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/createspace-independent-publishing-platform\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/ctek\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/ctek-power-inc\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/ctek-power-inc\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/ctek-power-inc\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/ctek-power-inc\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_model\/ctk56308\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/ctk56308\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/ctk56308\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_languages\/deutsch-published-deutsch-original-deutsch-unbekannt\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_languages\/deutsch-subtitled-englisch-original-dolby-digital-2-0\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/dorling-kindersley\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/dorling-kindersley\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/dorling-kindersley\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/dorling-kindersley\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/dorling-kindersley\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/dvd\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/dvd-blu-ray\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/elektronik\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/gebundene-ausgabe\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/generic\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_author\/gerhard-buzek\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_creator\/gerhard-buzek-zeichner-e-buzek-zeichner\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/grocery\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/grocery\/","http:\/\/localhost\/notruck\/app\/index.php\/category\/faq\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/haushaltswaren\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/hawk-gears\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/hawk-gears\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/hawk-gears\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/hawk-gears\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/hawk-outdoors\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_color\/helloliv-dunkeloliv\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/hghm-01\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/hghm-01\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/home\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/home-improvement\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/intipal\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/intipal\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/intipal\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/intipal\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/intipal\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/jarbo\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/jarbo\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/jarbo\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/jarbo\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/jarbo\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_catalognumberlist\/jbradio\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_model\/jbradio\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/jbradio\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/jbradio\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_author\/joe-mcmiller\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_author\/johannes-vogel\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_legaldisclaimer\/keine-garantie-oder-ruecknahme\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/kueche-haushalt\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/lanming\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/lanming\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/lanming\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/lanming\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/lanming\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/lawn-patio\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/lebensmittel-getraenke\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/liebeskummerpillen\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/liebeskummerpillen\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/liebeskummerpillen\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/liebeskummerpillen\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/meiermed-aerocasea\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/mih\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/mih\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/mih\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/mih\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/misc\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/nc1132-944\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/nc1132-944\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_catalognumberlist\/new-209687\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/nikol\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/nikol\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/nikol\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/nikol\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/outdoor_living\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/pet-products\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/pet_supplies\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/pietsch\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/pietsch\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/pietsch\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/pietsch\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/pietsch\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/plumbing_fixture\/","http:\/\/localhost\/notruck\/app\/index.php\/type\/image\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_legaldisclaimer\/privat-verkauf\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/pro-fun-media\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/pro-fun-media\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/pro-fun-media\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/pro-fun-media\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_model\/pro1r-bl1\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/pro1r-bl1\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/pro1r-bl1\/","http:\/\/localhost\/notruck\/app\/index.php\/category\/produkt-bericht\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/radio\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/red-cherry\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/red-cherry\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/red-cherry\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/red-cherry\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/red-cherry\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_creator\/scott-marlowe-hauptdarsteller-matthew-risch-hauptdarsteller\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/semptec-urban-survival-technology\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/semptec-urban-survival-technology\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/semptec-urban-survival-technology\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/semptec-urban-survival-technology\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/semptec-urban-survival-technology\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_producttypename\/sporting_goods\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_productgroup\/sports\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_model\/t6239\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_mpn\/t6239\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_partnumber\/t6239\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_binding\/taschenbuch\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/tetra\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/tetra\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/tetra\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/tetra\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/tetra\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/the-friendly-swede\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/the-friendly-swede\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/the-friendly-swede\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/the-friendly-swede\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/the-friendly-swede\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/unbekannt\/","http:\/\/localhost\/notruck\/app\/index.php\/category\/uncategorized\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_brand\/walther\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_label\/walther\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_manufacturer\/walther\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_publisher\/walther\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_studio\/walther\/","http:\/\/localhost\/notruck\/app\/index.php\/pa_catalognumberlist\/z00553741\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/advanced-empfohlen\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/advanced-grundausstattung\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/advanced-optional\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/basic-empfohlen\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/basic-grundausstatung\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/basic-optional\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/deluxe-empfohlen\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/deluxe-grundausstattung\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/deluxe-optional\/","http:\/\/localhost\/notruck\/app\/index.php\/produktkategorie\/advanced-grundausstattung\/hygiene\/","http:\/\/localhost\/notruck\/app\/index.php\/2015\/11\/","http:\/\/localhost\/notruck\/app\/index.php\/2015\/10\/","http:\/\/localhost\/notruck\/app\/index.php\/2015\/07\/","http:\/\/localhost\/notruck\/app\/?s=.","http:\/\/localhost\/notruck\/app\/?s=asdfasdfasdfasdf","http:\/\/localhost\/notruck\/app\/asdfasdfasdfasdf"], ignore: [
                    /(#|\.)fancybox(\-[a-zA-Z]+)?/, // for jQuery Fancybox
                    /(\.wf(\-[a-zA-Z]+)?)/,         // for webfontloader
                    // Bootstrap selectors added via JS
                    /\w\.in/,
                    ".fade",
                    ".collapse",
                    "/collapsing/",
                    /(#|\.)navbar(\-[a-zA-Z]+)?/,
                    /(#|\.)dropdown(\-[a-zA-Z]+)?/,
                    /(#|\.)(open)/,
                    // injected via JS
                    /disabled/,
                    /fa-chevron-up/,
                    // currently only in a IE conditional, so uncss doesn't see it
                    ".close",
                    ".alert-dismissible",
                    ".modal-dialog",
                    ".modal-body",
                    ".animated",
                     /(#|\.)(animated)/,
                    "overflow-y",
                    /\.modal/,
                     /(#|\.)(animated)/,
                      /(#|\.)(bounceInDown)/,
                       /(#|\.)(bounceInUp)/,
                       /(#|\.)(zoomInLeft)/
                ],
                  ignoreSheets: [/fonts.googleapis/]
              
            
          
          
         }) */

    .pipe(minifyCss, {
      advanced: false,
      rebase: false
    })
   
    .pipe(function() {
      return gulpif(enabled.rev, rev());
    })
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.write('.', {
        sourceRoot: 'assets/styles/'
      }));
    })();
};

// ### JS processing pipeline
// Example
// ```
// gulp.src(jsFiles)
//   .pipe(jsTasks('main.js')
//   .pipe(gulp.dest(path.dist + 'scripts'))
// ```
var jsTasks = function(filename) {
  return lazypipe()
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.init());
    })
    .pipe(concat, filename)
    .pipe(uglify, {
      compress: {
        'drop_debugger': enabled.stripJSDebug
      }
    })
    .pipe(function() {
      return gulpif(enabled.rev, rev());
    })
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.write('.', {
        sourceRoot: 'assets/scripts/'
      }));
    })();
};

// ### Write to rev manifest
// If there are any revved files then write them to the rev manifest.
// See https://github.com/sindresorhus/gulp-rev
var writeToManifest = function(directory) {
  return lazypipe()
    .pipe(gulp.dest, path.dist + directory)
    .pipe(browserSync.stream, {match: '**/*.{js,css}'})
    .pipe(rev.manifest, revManifest, {
      base: path.dist,
      merge: true
    })
    .pipe(gulp.dest, path.dist)();
};

// ## Gulp tasks
// Run `gulp -T` for a task summary

// ### Styles
// `gulp styles` - Compiles, combines, and optimizes Bower CSS and project CSS.
// By default this task will only log a warning if a precompiler error is
// raised. If the `--production` flag is set: this task will fail outright.
gulp.task('styles', ['wiredep'], function() {
  var merged = merge();
  manifest.forEachDependency('css', function(dep) {
    var cssTasksInstance = cssTasks(dep.name);
    if (!enabled.failStyleTask) {
      cssTasksInstance.on('error', function(err) {
        console.error(err.message);
        this.emit('end');
      });
    }
    merged.add(gulp.src(dep.globs, {base: 'styles'})
      .pipe(cssTasksInstance));
  });
  return merged
    .pipe(writeToManifest('styles'));
});

// ### Scripts
// `gulp scripts` - Runs JSHint then compiles, combines, and optimizes Bower JS
// and project JS.
gulp.task('scripts', ['jshint'], function() {
  var merged = merge();
  manifest.forEachDependency('js', function(dep) {
    merged.add(
      gulp.src(dep.globs, {base: 'scripts'})
        .pipe(jsTasks(dep.name))
    );
  });
  return merged
    .pipe(writeToManifest('scripts'));
});

// ### Fonts
// `gulp fonts` - Grabs all the fonts and outputs them in a flattened directory
// structure. See: https://github.com/armed/gulp-flatten
gulp.task('fonts', function() {
  return gulp.src(globs.fonts)
    .pipe(flatten())
    .pipe(gulp.dest(path.dist + 'fonts'))
    .pipe(browserSync.stream());
});

// ### Images
// `gulp images` - Run lossless compression on all the images.
gulp.task('images', function() {
  return gulp.src(globs.images)
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
    }))
    .pipe(gulp.dest(path.dist + 'images'))
    .pipe(browserSync.stream());
});

// ### JSHint
// `gulp jshint` - Lints configuration JSON and project JS.
gulp.task('jshint', function() {
  return gulp.src([
    'bower.json', 'gulpfile.js'
  ].concat(project.js))
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(gulpif(enabled.failJSHint, jshint.reporter('fail')));
});

// ### Clean
// `gulp clean` - Deletes the build folder entirely.
gulp.task('clean', require('del').bind(null, [path.dist]));

// ### Watch
// `gulp watch` - Use BrowserSync to proxy your dev server and synchronize code
// changes across devices. Specify the hostname of your dev server at
// `manifest.config.devUrl`. When a modification is made to an asset, run the
// build step for that asset and inject the changes into the page.
// See: http://www.browsersync.io
gulp.task('watch', function() {
  browserSync.init({
    files: ['{lib,templates}/**/*.php', '*.php'],
    proxy: config.devUrl,
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  gulp.watch([path.source + 'styles/**/*'], ['styles']);
  gulp.watch([path.source + 'scripts/**/*'], ['jshint', 'scripts']);
  gulp.watch([path.source + 'fonts/**/*'], ['fonts']);
  gulp.watch([path.source + 'images/**/*'], ['images']);
  gulp.watch(['bower.json', 'assets/manifest.json'], ['build']);
});

// ### Build
// `gulp build` - Run all the build tasks but don't clean up beforehand.
// Generally you should be running `gulp` instead of `gulp build`.
gulp.task('build', function(callback) {
  runSequence('styles',
              'scripts',
              ['fonts', 'images'],
              callback);
});

// ### Wiredep
// `gulp wiredep` - Automatically inject Less and Sass Bower dependencies. See
// https://github.com/taptapship/wiredep
gulp.task('wiredep', function() {
  var wiredep = require('wiredep').stream;
  return gulp.src(project.css)
    .pipe(wiredep())
    .pipe(changed(path.source + 'styles', {
      hasChanged: changed.compareSha1Digest
    }))
    .pipe(gulp.dest(path.source + 'styles'));
});

// ### Gulp
// `gulp` - Run a complete build. To compile for production run `gulp --production`.
gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
