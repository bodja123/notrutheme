
<div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <!-- Item 1 -->
            <div class="item active slide1">
                <div class="row"><div class="container">
                    <div class="col-md-3 text-right">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="<?php bloginfo('template_directory'); ?>/dist/images/11.png">
                    </div>
                    <div class="col-md-9 text-left" style="max-width: 60%;" >
                        <h3 data-animation="animated bounceInDown">Notfallrucksack gepackt?</h3>
                        <h4 data-animation="animated bounceInUp">Seien sie vorbereitet!</h4>             
                     </div>
                </div></div>
             </div> 
            <!-- Item 2 -->
            <div class="item slide2">
                <div class="row"><div class="container">
                    <div class="col-md-7 text-left"  style="max-width: 60%;">
                        <h3 data-animation="animated bounceInDown">Bundesamt für Bevölkerungsschutz und Katastrophenhilfe empfiehlt:</h3>
                        <h4 data-animation="animated bounceInUp">Packen sie ihren Notfallkoffer!</h4>
                     </div>
                    <div class="col-md-5 text-right">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft"  src="<?php bloginfo('template_directory'); ?>/dist/images/22.png">
                    </div>
                </div></div>
            </div>
            <!-- Item 3 -->
            <div class="item slide3"> 
                <div class="row"><div class="container">
                    <div class="col-md-7 text-left"  style="max-width: 60%;">
                        <h3 data-animation="animated bounceInDown">die wichtigsten Unterlagen</h3>
                        <h4 data-animation="animated bounceInUp">griffbereit im Notfall</h4>
                     </div>
                    <div class="col-md-5 text-right">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="<?php bloginfo('template_directory'); ?>/dist/images/33.png">
                    </div>     
                </div></div>
            </div>
            <!-- Item 4 -->
            <div class="item slide4">
                <div class="row"><div class="container">
                    <div class="col-md-7 text-left"  style="max-width: 60%;">
                        <h3 data-animation="animated bounceInDown">Nahrungsmittelvorräte</h3>
                        <h4 data-animation="animated bounceInUp">bereits gepackt?</h4>
                     </div>
                    <div class="col-md-5 text-right">
                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="<?php bloginfo('template_directory'); ?>/dist/images/44.png">
                    </div>  
                </div></div>
            </div>
            <!-- End Item 4 -->
    
        </div>
        <!-- End Wrapper for slides-->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-double-left"></i><span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-double-right"></i><span class="sr-only">Next</span>
        </a>
    </div>
</div>


