 <div class="row">
 
 
 <?php $query = new WP_Query( 'cat=550,posts_per_page => 16' ); ?>
 <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
 <div class="col-sm-6 col-md-4">
 <div class="post">
 
 <!-- Display the Title as a link to the Post's permalink. -->
 <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>

 <!-- Display the date (November 16th, 2009 format) and a link to other posts by this posts author. -->
 <small><?php the_time( 'j F, Y' ); ?> von <?php the_author_posts_link(); ?></small>
 </br><?php the_post_thumbnail('thumbnail'); ?>
  <div class="entry">
  	<?php the_excerpt(); ?>
  </div>

  <p class="postmetadata"><?php _e( 'Posted in' ); ?> <?php the_category( ', ' ); ?></p>
 </div> <!-- closes the first div box -->
</div>
 <?php endwhile; 
 wp_reset_postdata();
 else : ?>
 <p><?php _e( 'Sorry, noch keine Posts verfügbar!' ); ?></p>
   
 <?php endif; ?>
                
                
</div>