
<div class="panel panel-default ">
  <div class="panel-heading center deluxe">
    <h1 class="panel-title">DELUXE</h1>
      
  </div>

</div>
    
    <div class="thumbnail  center div">
      
      <img src="<?php bloginfo('template_directory'); ?>/dist/images/deluxe.svg" alt="...">       
              
      <div class="caption smallpadding" >

<div class="panel-group " id="accordion_t" role="tablist" aria-multiselectable="true">
<div class="circle cthird" id="circle_third"></div>
  <div class="panel panel-default noborder">
    <div class="panel-heading " role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion_t" href="#collapseOne_t" aria-expanded="true" aria-controls="collapseOne_t">
         Grundausstattung
        </a>
      </h4>
    </div>
    <div id="collapseOne_t" class="panel-collapse collapse in " role="tabpanel" aria-labelledby="headingOne_t">
      <div class="panel-body smallpadding ">
          <ul class="list-group checked-list-box">
       <?php
        $args = array( 'post_type' => 'product', 'product_cat' => 'deluxe-grundausstattung', 'orderby' => 'rand' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

                 <li class="list-group-item noborder" id="myDiv" data-checked="true">   
                     <div class="row">
                     <table><tr>
<td class="tdwidht">
                  <a data-toggle="modal" data-target="#myModalP" id="<?php echo get_permalink( $loop->post->ID ) ?>" onClick="reply_click(this.id)" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array( 40, 40, ), array( 'class' => 'alignleft' )); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" style="max-width: 10%;" />'; ?>

                       <small><?php the_title(); ?></small>  </a>

                        <span class="badge"><?php echo $product->get_price_html(); ?> </span>  <td class="tdwidht">   <div class="material-switch pull-right">
                             <div class ="checkbox">    <label style="font-size: 1.5em">     <input id="<?php echo $product->get_price(); ?>" name="deluxe" value="<?php the_id(); ?>" type="checkbox" checked="checked" onchange="toggleCheckbox_third(this)"/>
                             <span class="cr"><i class="cr-icon fa fa-check"></i></span></div>
                        </div>    </td>            
</tr></table>

                   

               </div> </li>

    <?php endwhile; ?>
              <?php wp_reset_query(); ?> </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default noborder">
    <div class="panel-heading" role="tab" id="headingTwo_t">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_t" href="#collapseTwo_t" aria-expanded="false" aria-controls="collapseTwo_t">
          Optional<i class="fa fa-folder-open" aria-hidden="true"></i>
        </a>
      </h4>
    </div>
    <div id="collapseTwo_t" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_t">
      <div class="panel-body smallpadding">
            <ul class="list-group checked-list-box">
      <?php
        $args = array( 'post_type' => 'product', 'product_cat' => 'deluxe-optional', 'orderby' => 'rand' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
 
                 <li class="list-group-item noborder" id="myDiv" data-checked="true">   
                     <div class="row">
                     <table><tr>
<td class="tdwidht">
                   <a data-toggle="modal" data-target="#myModalP" id="<?php echo get_permalink( $loop->post->ID ) ?>" onClick="reply_click(this.id)" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, array( 40, 40, ), array( 'class' => 'alignleft' )); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" style="max-width: 10%;" />'; ?>

                       <small><?php the_title(); ?></small>  </a>

                        <span class="badge"><?php echo $product->get_price_html(); ?> </span>  <td class="tdwidht">   <div class="material-switch pull-right">
                             <div class ="checkbox">    <label style="font-size: 1.5em">     <input id="<?php echo $product->get_price(); ?>" name="deluxe" value="<?php the_id(); ?>" type="checkbox"  onchange="toggleCheckbox_third(this)"/>
                             <span class="cr"><i class="cr-icon fa fa-check"></i></span></div>
                        </div>    </td>            
</tr></table>

                   

               </div> </li>
  <?php endwhile; ?>
              <?php wp_reset_query(); ?> </ul>
      </div>
    </div>
  </div>
 
</div>
 <p ><button type="button" onclick="checkbox_test()" class="btn btn-success" >
     <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>  in den Warenkorb </button>
   </p></form>


      </div>
    </div>
<script>

 var counter_third = 0, 
        i = 0,      
        price_third = 0,   
        // get a collection of objects with the specified 'input' TAGNAME
        input_obj = document.getElementsByTagName('input');
    // loop through all collected objects
    for (i = 0; i < input_obj.length; i++) {
        // if input object is checkbox and checkbox is checked then ...
        if (input_obj[i].name === 'deluxe' && input_obj[i].checked === true) {
            // ... increase counter and concatenate checkbox value to the url string
            counter++;
            price_third += +input_obj[i].id;
        }
    }



  var sum_third=price_third;

function toggleCheckbox_third(element){
        var number = element.id;
        if (element.checked)
            sum_third += Number(number);
        else
            sum_third -= Number(number);
  
        document.getElementById("circle_third").innerHTML = "€"+ Math.round(sum_third* 100) / 100;
} 


document.getElementById("circle_third").innerHTML = "€"+Math.round(sum_third* 100) / 100;









// function will loop through all input tags and create
// url string from checked checkboxes
function checkbox_test_third() {
    var counter = 0, // counter for checked checkboxes
        i = 0,       // loop variable
        url = '?add-to-cart=0',    // final url string
        // get a collection of objects with the specified 'input' TAGNAME
        input_obj = document.getElementsByTagName('input');
    // loop through all collected objects
    for (i = 0; i < input_obj.length; i++) {
        // if input object is checkbox and checkbox is checked then ...
        if (input_obj[i].name === 'deluxe' && input_obj[i].checked === true) {
            // ... increase counter and concatenate checkbox value to the url string
            counter++;
            url = url + ',' + input_obj[i].value;
        }
    }
    // display url string or message if there is no checked checkboxes
    if (counter > 0) {
        // remove first "&" from the generated url string
        url = url.substr(0);
        // display final url string
       // alert(url);
        // or you can send checkbox values
        window.location.href =  url; 
    }
    else {
        alert('Bitte wählen Sie mindestens ein Produkt aus!');
    }
}

 </script>