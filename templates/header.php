<header>
           <!-- Modal Cart -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
             <button type="button" style="background-color:transparent; margin-bottom:-30px; border:none;" class="fa  fa-times fa-2x" data-dismiss="modal"></button>
                  <div class="modal-body">
                <?php echo do_shortcode(' [do_widget "WooCommerce Warenkorb" ]'); ?>
                  </div>
                
                </div>
              </div>
            </div>
    <!-- Modal Search -->
            <div class="modal fade" id="myModalS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
             <button type="button" style="background-color:transparent; margin-bottom:-30px; border:none;" class="fa  fa-times fa-2x" data-dismiss="modal"></button>
                  <div class="modal-body product">
  <ul style=" list-style-type: none;">
    <li><?php echo do_shortcode('   [wcas-search-form] '); ?></li> 
      <li ><?php echo do_shortcode(' [do_widget "WooCommerce Produktschlagwörter" ]'); ?></li>
          <li ><?php echo do_shortcode(' [do_widget "XO10 - WooCommerce Categories" ]'); ?></li>
      
    
  </ul>
                  </div>
                
                </div>
              </div>
            </div>
                <!-- Modal Procduct -->
            <div class="modal fade" id="myModalP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog"  style="width:95%;height: 95%;">
                <div class="modal-content"  style="height: 95%;">
             <button type="button" style="background-color:transparent; margin-bottom:-30px; border:none;" class="fa  fa-times fa-2x" data-dismiss="modal"></button>
                  <div class="modal-body"  style="height: 95%;">
               
  <iframe id="pframe" style="height:100%;width:90%" frameBorder="0" src=""></iframe>
                  </div>
                
                </div>
              </div>
            </div>
            

<nav class="navbar navbar-default" role="navigation">
    
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
      
    <div class="navbar-header">
      
      
      <button type="button" class="navbar-toggle noborder" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Navigation ein-/ausblenden</span>
        <span class="fa fa-bars fa-2x"></span>
   
      </button>
         
     <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><img class="logo" src="<?php bloginfo('template_directory'); ?>/dist/images/logo.jpg" alt="" /> </a>
      
        
        
    
            
     <a href="<?php bloginfo('url'); ?>"> <h1>notfallrucksack-online.de</h1></a><h1 style="color:white; font-size:2.0em;">notfallrucksack-online</h1>
        
       </div>
          
 <div class="searchform hidden-xs ">
   
  <button class="btn btn-default  btn-lg noborder" type="button" data-toggle="modal" data-target="#myModal">
   <span class=" fa fa-shopping-cart fa-2x " aria-hidden="true"></span> 
  
  </button>

</div>  <div class="searchform hidden-xs">
  
 <button class="btn btn-default  btn-lg noborder" type="button" data-toggle="modal" data-target="#myModalS">  <span class=" fa fa-search fa-2x " aria-hidden="true"></span> 

  </button>
  
</div>  

		

     
   

        <?php
            wp_nav_menu( array(
                'menu'              => 'primary_navigation',
                'theme_location'    => 'primary_navigation',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navmargin',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())

            );
       ?>
     
    </div>
 
  
</nav>
    </div>
          

</header>

 
